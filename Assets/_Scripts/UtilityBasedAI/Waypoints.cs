﻿using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour
{
	private List<Transform> waypoints = new List<Transform>();

	void Awake()
	{
		foreach (Transform waypoint in transform)
		{
			waypoints.Add(waypoint);
		}
	}

	public Vector3 GetRandomWaypoint(int oldidx, AgentUtilityAI agent)
	{
		Vector3 waypoint;
		int wpIdx = Random.Range(0, waypoints.Count - 1);
		while (wpIdx == oldidx)
		{
			wpIdx = Random.Range(0, waypoints.Count - 1);
		}

		agent.OldWaypointIdx = wpIdx;
		waypoint = new Vector3(waypoints[wpIdx].position.x, 0f, waypoints[wpIdx].position.z);

		return waypoint;
	}
}