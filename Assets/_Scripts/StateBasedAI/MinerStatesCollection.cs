﻿using System.Collections.Generic;
using UnityEngine;

public class MinerStatesCollection : MonoBehaviour
{
	[SerializeField] private Transform mine;
	public Transform Mine
	{
		get { return mine; }
		set { mine = value; }
	}
	[SerializeField] private Transform home;
	public Transform Home
	{
		get { return home; }
		set { home = value; }
	}
	[SerializeField] private Transform saloon;
	public Transform Saloon
	{
		get { return saloon; }
		set { saloon = value; }
	}
	[SerializeField] private Transform bank;
	public Transform Bank
	{
		get { return bank; }
		set { bank = value; }
	}

	public List<Transform> mineSlots;
	public bool[] mineSlotUsed;
	public List<Transform> bankSlots;
	public bool[] bankSlotUsed;
	public List<Transform> saloonSlots;
	public bool[] saloonSlotUsed;
	public List<Transform> homeSlots;
	public bool[] homeSlotUsed;

	void Start()
	{
			foreach (Transform slot in Mine.transform)
			{
				mineSlots.Add(slot);
			}
			foreach (Transform slot in Bank.transform)
			{
				bankSlots.Add(slot);
			}
			foreach (Transform slot in Saloon.transform)
			{
				saloonSlots.Add(slot);
			}
			foreach (Transform slot in Home.transform)
			{
				homeSlots.Add(slot);
			}

			mineSlotUsed = new bool[mineSlots.Count];
			bankSlotUsed = new bool[bankSlots.Count];
			saloonSlotUsed = new bool[saloonSlots.Count];
			homeSlotUsed = new bool[homeSlots.Count];
	}
}

public class Mine : MinerStates
{
	public override MinerStateAI minerAI { get; protected set; }
	public override MinerStatesCollection stateCollection { get; protected set; }
	private int mineAmount = 0;
	private int oldMineAmount = 0;
	private int bagLimit = 20;
	private float timer;
	private float thirst;

	public Mine(MinerStateAI miner, MinerStatesCollection statesCollection) : base(miner, statesCollection)
	{

	}

	public override void OnStateEnter()
	{
		timer = 0f;
		mineAmount = oldMineAmount;
		minerAI.MinerText.text = $"I arrived at the mine! I still have {mineAmount} from my previous mining!";
	}

	public override void OnStateExit()
	{
		timer = 0f;
		mineAmount = oldMineAmount;
		minerAI.FreeSlot();
	}

	public override void OnUpdate()
	{
		timer += Time.deltaTime;
		thirst += Time.deltaTime;

		if (thirst < 15f)
		{
			if (timer > 1f)
			{
				// mine a random amount
				int mineAmountCount = Random.Range(1, 3);
				mineAmount += mineAmountCount;
				minerAI.MinerText.text = $"Oh, I found {mineAmountCount} crystals! I have {mineAmount} / {bagLimit} crystals";
				if (mineAmount >= bagLimit)
				{
					minerAI.MinerText.text = "My bag is full! Let's turn them in at the bank.";
					mineAmount = bagLimit;
					minerAI.SetTargetLocation(stateCollection.Bank);
					minerAI.ChangeState(minerAI.travelling);
				}
				timer = 0f;
			}
		}
		else
		{
			minerAI.MinerText.text = "I'm very thirsty. I need to drink!";
			thirst = 0f;
			oldMineAmount = mineAmount;
			minerAI.SetTargetLocation(stateCollection.Saloon);
			minerAI.ChangeState(minerAI.travelling);
		}
	}
}

public class Saloon : MinerStates
{
	public override MinerStateAI minerAI { get; protected set; }
	public override MinerStatesCollection stateCollection { get; protected set; }
	private float timer;

	public Saloon(MinerStateAI miner, MinerStatesCollection statesCollection) : base(miner, statesCollection)
	{

	}

	public override void OnStateEnter()
	{
		minerAI.MinerText.text = "I arrived at the saloon!";
		timer = 5f;
	}

	public override void OnStateExit()
	{
		minerAI.FreeSlot();
	}

	public override void OnUpdate()
	{
		timer -= Time.deltaTime;
		minerAI.MinerText.text = $"I still need to drink for another {timer.ToString("0.00")} seconds!";
		if (timer < 0f)
		{
			minerAI.MinerText.text = "Ahh! Now let's get back to work!";
			minerAI.SetTargetLocation(stateCollection.Mine);
			minerAI.ChangeState(minerAI.travelling);
		}
	}
}

public class Bank : MinerStates
{
	public override MinerStateAI minerAI { get; protected set; }
	public override MinerStatesCollection stateCollection { get; protected set; }
	private int dailyAmount = 0;
	private int dailyGoal = 60;
	private float timer;

	public Bank(MinerStateAI miner, MinerStatesCollection statesCollection) : base(miner, statesCollection)
	{

	}

	public override void OnStateEnter()
	{
		timer = 0f;
	}

	public override void OnStateExit()
	{
		timer = 0f;
		minerAI.FreeSlot();
	}

	public override void OnUpdate()
	{
		timer += Time.deltaTime;

		if (timer > 3f)
		{
			dailyAmount += 20;
			minerAI.MinerText.text = $"I have already mined {dailyAmount} / {dailyGoal} crystals!";

			// if the daily goal is reached travel home
			if (dailyAmount >= dailyGoal)
			{
				minerAI.MinerText.text = "I've reached my goal and i'm tired. Let's get some.";
				minerAI.SetTargetLocation(stateCollection.Home);
				minerAI.ChangeState(minerAI.travelling);
			}
			else
			{
				minerAI.MinerText.text = "I still need to collect more crystals!";
				minerAI.SetTargetLocation(stateCollection.Mine);
				minerAI.ChangeState(minerAI.travelling);
			}
		}
	}
}

public class Home : MinerStates
{
	public override MinerStateAI minerAI { get; protected set; }
	public override MinerStatesCollection stateCollection { get; protected set; }
	private float rest = 5f;

	public Home(MinerStateAI miner, MinerStatesCollection statesCollection) : base(miner, statesCollection)
	{

	}

	public override void OnStateEnter()
	{
		rest = 5f;
	}

	public override void OnStateExit()
	{
		rest = 5f;
		minerAI.FreeSlot();
	}

	public override void OnUpdate()
	{
		// if the rest is back up go mining
		rest -= Time.deltaTime;
		minerAI.MinerText.text = $"I need to rest for another {rest.ToString("0.00")} seconds...";

		if (rest < 0f)
		{
			minerAI.MinerText.text = "Let's get to work again!";
			minerAI.SetTargetLocation(stateCollection.Mine);
			minerAI.ChangeState(minerAI.travelling);
		}
	}
}

public class Travelling : MinerStates
{
	public override MinerStateAI minerAI { get; protected set; }
	public override MinerStatesCollection stateCollection { get; protected set; }
	private Transform targetLocation;

	public Travelling(MinerStateAI miner, MinerStatesCollection statesCollection) : base(miner, statesCollection)
	{

	}

	public override void OnStateEnter()
	{
		targetLocation = minerAI.TargetLocation;
	}

	public override void OnStateExit()
	{
	}

	public override void OnUpdate()
	{
		// move to the target location
		Vector3 distance = new Vector3(targetLocation.position.x, 0f, targetLocation.position.z) - new Vector3(minerAI.transform.position.x, 0f, minerAI.transform.position.z) ;
		if (distance.sqrMagnitude < 0.1f)
		{
			minerAI.ChangeState(minerAI.currentBuildingState);
		}
		Vector3 dir = distance.normalized;
		minerAI.transform.position += dir * minerAI.Speed * Time.deltaTime;
	}
}

public class SelectEmptySlot : MinerStates
{
	public override MinerStateAI minerAI { get; protected set; }
	public override MinerStatesCollection stateCollection { get; protected set; }

	private bool alreadyCreatedLists;

	public SelectEmptySlot(MinerStateAI miner, MinerStatesCollection statesCollection) : base(miner, statesCollection)
	{

	}

	public override void OnStateEnter()
	{
		
	}

	public override void OnStateExit()
	{
	}

	public override void OnUpdate()
	{
		// check the state and then look for a free slot
		if (minerAI.currentBuildingState == minerAI.mine)
		{
			for (int i = 0; i < stateCollection.mineSlotUsed.Length; i++)
			{
				if (stateCollection.mineSlotUsed[i] == true)
				{
					continue;
				}
				else if (stateCollection.mineSlotUsed[i] == false)
				{
					// give the transform of this position to the travelling
					minerAI.MinerText.text = $"I'm going to use slot {i + 1}!";
					stateCollection.mineSlotUsed[i] = true;
					minerAI.usedSlotNumber = i;
					minerAI.SetTargetLocation(stateCollection.mineSlots[i]);
					minerAI.ChangeState(minerAI.travelling);
					break;
				}
			}
		}
		else if (minerAI.currentBuildingState == minerAI.bank)
		{
			for (int i = 0; i < stateCollection.bankSlotUsed.Length; i++)
			{
				if (stateCollection.bankSlotUsed[i] == true)
				{
					continue;
				}
				else
				{
					// give the transform of this position to the travelling
					minerAI.MinerText.text = $"I'm going to use slot {i + 1}!";
					stateCollection.bankSlotUsed[i] = true;
					minerAI.usedSlotNumber = i;
					minerAI.SetTargetLocation(stateCollection.bankSlots[i]);
					minerAI.ChangeState(minerAI.travelling);
					break;
				}
			}
		}
		else if (minerAI.currentBuildingState == minerAI.saloon)
		{
			for (int i = 0; i < stateCollection.saloonSlotUsed.Length; i++)
			{
				if (stateCollection.saloonSlotUsed[i] == true)
				{
					continue;
				}
				else
				{
					// give the transform of this position to the travelling
					minerAI.MinerText.text = $"I'm going to use slot {i + 1}!";
					stateCollection.saloonSlotUsed[i] = true;
					minerAI.usedSlotNumber = i;
					minerAI.SetTargetLocation(stateCollection.saloonSlots[i]);
					minerAI.ChangeState(minerAI.travelling);
					break;
				}
			}
		}
		else if (minerAI.currentBuildingState == minerAI.home)
		{
			for (int i = 0; i < stateCollection.homeSlotUsed.Length; i++)
			{
				if (stateCollection.homeSlotUsed[i] == true)
				{
					continue;
				}
				else
				{
					// give the transform of this position to the travelling
					minerAI.MinerText.text = $"I'm going to use slot {i + 1}!";
					stateCollection.homeSlotUsed[i] = true;
					minerAI.usedSlotNumber = i;
					minerAI.SetTargetLocation(stateCollection.homeSlots[i]);
					minerAI.ChangeState(minerAI.travelling);
					break;
				}
			}
		}
	}
}