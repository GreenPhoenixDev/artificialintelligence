﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinerController : MonoBehaviour
{
	[SerializeField] private GameObject minerPrefab;
	[SerializeField] private Transform minerSpawn;
	[SerializeField] private float timerMax;
	[SerializeField] private float yOffset;
	private int minerCount = 0;

	private float timer = 5f;

	void Start()
	{
	}

	void Update()
	{
		timer += Time.deltaTime;
	}

	public void SpawnMiner()
	{
		if (timer > timerMax)
		{
			//minerCount++;

			//if (minerCount < 4)
				Instantiate(minerPrefab, minerSpawn.position + new Vector3(0f, yOffset, 0f), minerSpawn.rotation);
			timer = 0f;
		}
	}
}
