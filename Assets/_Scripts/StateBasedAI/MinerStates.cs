﻿public abstract class MinerStates
{
	public abstract MinerStateAI minerAI { get; protected set; }
	public abstract MinerStatesCollection stateCollection { get; protected set; }
	public abstract void OnStateEnter();
	public abstract void OnStateExit();
	public abstract void OnUpdate();

	public MinerStates(MinerStateAI miner, MinerStatesCollection minerStatesCollection)
	{
		minerAI = miner;
		stateCollection = minerStatesCollection;
	}
}
