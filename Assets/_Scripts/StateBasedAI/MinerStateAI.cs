﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MinerStateAI : MonoBehaviour
{
	public MinerStates bank;
	public MinerStates saloon;
	public MinerStates mine;
	public MinerStates home;
	public MinerStates travelling;
	public MinerStates selectEmptySlot;
	public MinerStates currentState;
	public MinerStates currentBuildingState;

	[SerializeField] private TextMeshProUGUI minerText;
	public TextMeshProUGUI MinerText
	{
		get { return minerText; }
		set { minerText = value; }
	}
	private MinerStatesCollection msCol;

	private Transform targetLocation;
	public Transform TargetLocation
	{
		get { return targetLocation; }
		set { targetLocation = value; }
	}
	[SerializeField] private float speed;
	public float Speed
	{
		get { return speed; }
		set { speed = value; }
	}

	public int usedSlotNumber;

	#region Monobehaviour
	void Start()
	{
		msCol = GameObject.Find("MinerStatesCollection").GetComponent<MinerStatesCollection>();
		bank = new Bank(this, msCol);
		saloon = new Saloon(this, msCol);
		mine = new Mine(this, msCol);
		home = new Home(this, msCol);
		selectEmptySlot = new SelectEmptySlot(this, msCol);
		travelling = new Travelling(this, msCol);
		currentState = home;
		currentBuildingState = home;
	}
	void Update()
	{
		currentState.OnUpdate();
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.name == "BankTriggerzone")
		{
			currentBuildingState = bank;
			ChangeState(selectEmptySlot);
		}
		else if (other.gameObject.name == "MineTriggerzone")
		{
			currentBuildingState = mine;
			ChangeState(selectEmptySlot);
		}
		else if (other.gameObject.name == "SaloonTriggerzone")
		{
			currentBuildingState = saloon;
			ChangeState(selectEmptySlot);
		}
		else if (other.gameObject.name == "HomeTriggerzone")
		{
			currentBuildingState = home;
			ChangeState(selectEmptySlot);
		}
	}
	#endregion

	#region StateFunctions
	public void ChangeState(MinerStates state)
	{
		currentState.OnStateExit();
		currentState = state;
		currentState.OnStateEnter();
	}
	public void SetTargetLocation(Transform _targetLocation)
	{
		targetLocation = _targetLocation;
	}
	public void FreeSlot()
	{
		if(currentBuildingState == mine)
		{
			msCol.mineSlotUsed[usedSlotNumber] = false;
		}
		else if (currentBuildingState == bank)
		{
			msCol.bankSlotUsed[usedSlotNumber] = false;
		}
		else if (currentBuildingState == saloon)
		{
			msCol.saloonSlotUsed[usedSlotNumber] = false;
		}
		else if (currentBuildingState == home)
		{
			msCol.homeSlotUsed[usedSlotNumber] = false;
		}
	}
	#endregion
}
