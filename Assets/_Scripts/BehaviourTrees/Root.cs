﻿namespace BehaviourTree
{
	public class Root : Task
	{
		private Task childTask;

		public Root(Task _childTask)
		{
			childTask = _childTask;
		}

		public override Status Update(Context context)
		{
			Context<AgentData> c = (Context<AgentData>)context;
			c.data.Debug.text = "";
			var status = childTask.Update(context);
			c.data.Debug.text += $"root status : {status}";
			return status;
		}
	}
}
