﻿using System.Collections.Generic;

namespace BehaviourTree
{
	public class Sequence : Composite
	{
		private int curChildIdx;

		public Sequence(List<Task> childrenTasks) : base(childrenTasks)
		{
		}

		public override Status Update(Context context)
		{
			Context<AgentData> c = (Context<AgentData>)context;
			while (curChildIdx < childrenTasks.Count)
			{
				switch (childrenTasks[curChildIdx].Update(context))
				{
					case Status.Success:
						if (++curChildIdx < childrenTasks.Count)
						{
							c.data.Debug.text += $"sequence : continue\n";
							continue;
						}
						else
						{
							c.data.Debug.text += $"sequence : success\n";
							curChildIdx = 0;
							return Status.Success;
						}

					case Status.Failure:
						c.data.Debug.text += $"sequence : fail\n";
						curChildIdx = 0;
						return Status.Failure;

					case Status.Running:
						c.data.Debug.text += $"sequence : running\n";
						return Status.Running;

					default:
						throw new System.Exception("sequence : unexpected child status return");
				}
			}
			throw new System.Exception("sequence : unexpected termination out of loop");
		}
	}
}
