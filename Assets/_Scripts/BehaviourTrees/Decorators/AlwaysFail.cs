﻿namespace BehaviourTree
{
	public class AlwaysFail : Decorator
	{
		public AlwaysFail(Task _childTask) : base(_childTask)
		{
		}

		public override Status Update(Context context)
		{
			switch (childTask.Update(context))
			{
				case Status.Success:
				return Status.Failure;

				case Status.Failure:
				return Status.Failure;

				case Status.Running:
					return Status.Running;

				default:
					throw new System.Exception("alwaysFail : unexpected status");
			}
		}
	}
}
