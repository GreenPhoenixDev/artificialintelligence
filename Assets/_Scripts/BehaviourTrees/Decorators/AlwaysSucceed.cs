﻿namespace BehaviourTree
{
	public class AlwaysSucceed : Decorator
	{
		public AlwaysSucceed(Task _childTask) : base(_childTask)
		{
		}

		public override Status Update(Context context)
		{
			switch (childTask.Update(context))
			{
				case Status.Success:
					return Status.Success;

				case Status.Failure:
					return Status.Success;

				case Status.Running:
					return Status.Running;

				default:
					throw new System.Exception("alwaysSucceed : unexpected status");
			}
		}
	}
}
