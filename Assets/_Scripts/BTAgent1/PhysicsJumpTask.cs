﻿using UnityEngine;

namespace BehaviourTree
{
	public class PhysicsJumpTask : Task
	{
		private float force;

		public PhysicsJumpTask(float _force)
		{
			force = _force;
		}

		public override Status Update(Context context)
		{
			Context<AgentData> c = (Context<AgentData>)context;
			c.data.Debug.text += $"jump : success\n";
			c.data.Rb.AddForce(Vector3.up * force, ForceMode.Impulse);
			return Status.Success;
		}
	}
}
