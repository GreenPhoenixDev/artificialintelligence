﻿using UnityEngine;

namespace BehaviourTree
{
	public class WaitTask : Task
	{
		private float duration;
		private float timer;

		public WaitTask(float _duration)
		{
			duration = _duration;
		}

		public override Status Update(Context context)
		{
			Context<AgentData> c = (Context<AgentData>)context;
			timer += Time.deltaTime;
			c.data.Debug.text += $"timer : {timer.ToString("0.0")}\n";
			if (timer >= duration)
			{
				c.data.Debug.text += $"wait done\n";
				timer = 0f;
				return Status.Success;
			}
			else
			{
				c.data.Debug.text += $"wait running\n";
				return Status.Running;
			}
		}
	}
}
