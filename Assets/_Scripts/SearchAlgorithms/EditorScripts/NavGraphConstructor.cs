﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class NavGraphConstructor : MonoBehaviour
{
	public float handleSize = 1f;
	public Color circlehandleCapColor;

	[SerializeField] private NavGraph navGraph;

	#region MonoBehaviour
	void OnDrawGizmos()
	{
		//Debug.Log("working");
		//Handles.color = circlehandleCapColor;
		//Handles.FreeMoveHandle(transform.position, Quaternion.identity, handleSize, Vector3.one, Handles.DotHandleCap);
	}
	#endregion

	// foreach node in AllNodes draw a handle on their position
	// when the handle is moved, move the assigned object
}

[CustomEditor(typeof(NavGraphConstructor))]
public class NavGraphConstructorEditor : Editor
{
	void OnSceneGUI()
	{
		//NavGraph

		//Handles.
	}
}