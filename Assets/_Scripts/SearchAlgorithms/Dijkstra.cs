﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Priority_Queue;

public class Dijkstra : MonoBehaviour
{
	[SerializeField] private NavNode startNode;
	[SerializeField] private NavNode endNode;
	[SerializeField] private Color directionArrowColor;
	[SerializeField] private Color pathArrowColor;
	private List<NavNode> curRoute = new List<NavNode>();
	private NavGraph navGraph;
	Dictionary<NavNode, NavNode> cameFrom = new Dictionary<NavNode, NavNode>();

	#region Graph Evaluation
	public List<NavNode> GetPath()
	{
		navGraph = GetComponent<NavGraph>();
		SimplePriorityQueue<NavNode> frontier = new SimplePriorityQueue<NavNode>();
		frontier.Enqueue(startNode, 0f);
		Dictionary<NavNode, float> costSoFar = new Dictionary<NavNode, float>();
		cameFrom.Clear();
		cameFrom.Add(startNode, null);
		costSoFar.Add(startNode, 0f);
		NavNode curNode = null;

		while (frontier.Count > 0)
		{
			curNode = frontier.Dequeue();
			if (curNode == endNode) { break; }

			for (int i = 0; i < curNode.Edges.Count; i++)
			{
				Edge curEdge = curNode.Edges[i];
				float newCost = costSoFar[curNode] + curEdge.Cost;
				if(!costSoFar.ContainsKey(curEdge.ToNode) || newCost < costSoFar[curEdge.ToNode])
				{
					costSoFar[curEdge.ToNode] = newCost;
					frontier.Enqueue(curEdge.ToNode, newCost);
					cameFrom[curEdge.ToNode] = curNode;
				}
			}
		}

		if(curNode != endNode) { return null; }
		List<NavNode> route = new List<NavNode>();

		while (curNode != startNode)
		{
			route.Add(curNode);
			curNode = cameFrom[curNode];
		}

		route.Add(startNode);
		route.Reverse();
		curRoute = route;

		return route;
	}
	#endregion

	#region Scene UI
	private void OnDrawGizmos()
	{
		if (navGraph == null) { return; }
		if(cameFrom.Count < 1) { return; }

		// draw a path to the goal
		for (int i = 0; i < curRoute.Count - 1; i++)
		{
			Vector3 posFrom = curRoute[i].transform.position;
			Vector3 posTo = curRoute[i + 1].transform.position;

			Quaternion rot = Quaternion.LookRotation((posTo - posFrom).normalized);

			Handles.color = pathArrowColor;
			Handles.ArrowHandleCap(1, posFrom, rot, (posTo - posFrom).magnitude, EventType.Repaint);
		}
	}
	#endregion
}

[CustomEditor(typeof(Dijkstra))]
public class DijkstraEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		Dijkstra dijkstra = (Dijkstra)target;

		if (GUILayout.Button("Get Path"))
			dijkstra.GetPath();
	}
}