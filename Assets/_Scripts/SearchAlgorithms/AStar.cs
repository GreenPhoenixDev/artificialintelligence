﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Priority_Queue;

public class AStar : MonoBehaviour
{
	[SerializeField] private NavGraph navGraph;
	[SerializeField] private NavNode startNode;
	[SerializeField] private NavNode endNode;
	[SerializeField] private Color pathArrowColor;
	private SimplePriorityQueue<int, float> openSet = new SimplePriorityQueue<int, float>();
	private HashSet<int> closedSet = new HashSet<int>();
	private Dictionary<int, int> cameFrom = new Dictionary<int, int>();
	private Dictionary<int, float> gScores = new Dictionary<int, float>();
	private List<int> curPath;

	public List<int> GetPath()
	{
		openSet.Clear();
		closedSet.Clear();
		cameFrom.Clear();
		gScores.Clear();
		curPath.Clear();

		for (int i = 0; i < navGraph.AllNodes.Length; i++)
		{
			gScores.Add(i, Mathf.Infinity);
		}

		float g = 0f; // only for the start node
		float h = Vector3.Distance(startNode.transform.position, endNode.transform.position);
		openSet.Enqueue(startNode.Idx, g + h);
		gScores[startNode.Idx] = 0f;

		int curNodeIdx = 0;

		while (openSet.Count > 0)
		{
			curNodeIdx = openSet.First;

			if (curNodeIdx == endNode.Idx) { break; }

			curNodeIdx = openSet.Dequeue();
			closedSet.Add(curNodeIdx);

			for (int i = 0; i < navGraph.AllNodes[curNodeIdx].Edges.Count; i++)
			{
				Edge curEdge = navGraph.AllNodes[curNodeIdx].Edges[i];
				int curNeIdx = curEdge.ToNode.Idx;

				if (closedSet.Contains(curNeIdx))
					continue;

				if (!openSet.Contains(curNeIdx))
					openSet.Enqueue(curNeIdx, Mathf.Infinity);

				float tempG = gScores[curNodeIdx] + curEdge.Cost;

				if (tempG > gScores[curNeIdx])
					continue;

				gScores[curNeIdx] = tempG;
				cameFrom[curNeIdx] = curNodeIdx;

				float f = tempG + Vector3.Distance(navGraph.AllNodes[curNeIdx].transform.position, endNode.transform.position);

				if (!openSet.TryUpdatePriority(curNeIdx, f))
					throw new System.Exception("could not update priority!");
			}
		}

		List<int> path = new List<int>();
		//path.Add(curNodeIdx);
		while (curNodeIdx != startNode.Idx)
		{
			curNodeIdx = cameFrom[curNodeIdx];
			path.Add(curNodeIdx);
		}
		path.Reverse();
		curPath = path;
		return path;
	}

	void OnDrawGizmos()
	{
		if (navGraph == null) { return; }
		if (cameFrom.Count < 1) { return; }

		// draw a path to the goal
		for (int i = 0; i < curPath.Count - 1; i++)
		{
			Vector3 posFrom = navGraph.AllNodes[curPath[i]].transform.position;
			Vector3 posTo = navGraph.AllNodes[curPath[i + 1]].transform.position;

			Quaternion rot = Quaternion.LookRotation((posTo - posFrom).normalized);

			Handles.color = pathArrowColor;
			Handles.ArrowHandleCap(1, posFrom, rot, (posTo - posFrom).magnitude, EventType.Repaint);
		}
	}
}

[CustomEditor(typeof(AStar))]
public class AStarEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		AStar aStar = (AStar)target;

		if (GUILayout.Button("Get Path"))
			aStar.GetPath();
	}
}