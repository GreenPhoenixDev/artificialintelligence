# Articial Intelligence

In this project I tried some ways of creating basic game AI.

I've tried around with Search Algorithms ( Depth-First-Search, Breadth-First-Search, Dijkstra and A-Star), BehaviourTrees, State-Based-AI and Utility-Based-AI(not finished).


## Search Algorithms

![](https://imgur.com/4GZq5vy.png)

## Behaviour Trees

![](https://imgur.com/VoJFt6d.mp4)

## State Based AI

![](https://imgur.com/uvftvu8.mp4)